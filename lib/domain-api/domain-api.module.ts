import { Module, HttpModule, DynamicModule, Global } from '@nestjs/common';
import { DOMAINAPI_MODULE_OPTIONS } from './constants/domain-api.constants';
import { DomainAPIOptions } from './interfaces/domain-api-options.interface';
import { DomainApiService } from './services/domain-api.service';

@Global()
@Module({})
export class DomainApiModule {
  static forRoot(options?: DomainAPIOptions): DynamicModule {
    const domainAPIOption = {
      provide: DOMAINAPI_MODULE_OPTIONS,
      useValue: options,
    };
    return {
      imports: [HttpModule],
      module: DomainApiModule,
      providers: [DomainApiService, domainAPIOption],
      exports: [DomainApiService],
    };
  }
}
