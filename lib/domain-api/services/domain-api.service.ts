import { HttpService, Inject, Injectable } from '@nestjs/common';
import { AxiosResponse } from 'axios';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { DOMAINAPI_MODULE_OPTIONS } from '../constants/domain-api.constants';
import { DomainAPIOptions } from '../interfaces/domain-api-options.interface';

@Injectable()
export class DomainApiService {
    constructor(
        @Inject(DOMAINAPI_MODULE_OPTIONS)
        private readonly domaiAPIOption: DomainAPIOptions,
        private readonly httpService: HttpService,
    ) { }
    private formatErrors(error: any) {
        return observableThrowError(error);
    }
    post(path: string, body: any = {}): Observable<AxiosResponse> {
        return this.httpService.post(this.domaiAPIOption.host + path, body).pipe(
            catchError(this.formatErrors),
            map((res: AxiosResponse) => res),
        );
    }
    get(path: string, body: any = {}): Observable<AxiosResponse<any>> {
        const url = this.domaiAPIOption.host + path;
        return this.httpService.get(url).pipe(
            catchError(this.formatErrors),
            map(res => res),
        );
    }
    put(path: string, body: any = {}): Observable<AxiosResponse> {
        return this.httpService.put(this.domaiAPIOption.host + path, body).pipe(
            catchError(this.formatErrors),
            map((res: AxiosResponse) => res),
        );
    }
    delete(path: string, body: any = {}): Observable<AxiosResponse> {
        return this.httpService.delete(this.domaiAPIOption.host + path).pipe(
            catchError(this.formatErrors),
            map((res: AxiosResponse) => res),
        );
    }
}
