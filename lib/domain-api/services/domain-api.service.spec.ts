import { Test, TestingModule } from '@nestjs/testing';
import { DomainApiService } from './domain-api.service';
import { DOMAINAPI_MODULE_OPTIONS } from '../constants/domain-api.constants';
import { HttpModule, HttpService } from '@nestjs/common';
import { observable, of, Observable } from 'rxjs';
import { AxiosResponse } from 'axios';

const mockHttpService = {
  get:  jest.fn().mockReturnValue(of(new Observable<AxiosResponse>())) ,
  put:    jest.fn().mockReturnValue(of(new Observable<AxiosResponse>())) ,
  post:   jest.fn().mockReturnValue(of(new Observable<AxiosResponse>())) ,
  delete:  jest.fn().mockReturnValue(of(new Observable<AxiosResponse>())) ,
}
describe('DomainApiService', () => {
  let service: DomainApiService;

  beforeEach(async () => {
    const domainAPIOption = {
      host: 'http://localhost',
      port: 3000
    };
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [
        DomainApiService,
        {
          provide: DOMAINAPI_MODULE_OPTIONS,
          useValue: domainAPIOption,
        },
        {
          provide: HttpService,
          useValue: mockHttpService,
        },

      ]
    }).compile();

    service = module.get<DomainApiService>(DomainApiService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('Get Method call', () => {
    service.get("path");
    jest.spyOn(mockHttpService, 'get');
    expect(mockHttpService.get).toBeCalled();
  });


  it('Post Method call', () => {
    service.post("path");
    jest.spyOn(mockHttpService, 'get');
    expect(mockHttpService.post).toBeCalled();
  });

  it('Put Method call', () => {
    service.put("path");
    jest.spyOn(mockHttpService, 'get');
    expect(mockHttpService.put).toBeCalled();
  });

  it('Put Method call', () => {
    service.delete("path");
    jest.spyOn(mockHttpService, 'get');
    expect(mockHttpService.delete).toBeCalled();
  });

});
