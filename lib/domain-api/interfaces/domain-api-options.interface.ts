export interface DomainAPIOptions {
  host: string;
  port: number;
}
