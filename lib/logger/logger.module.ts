import { DynamicModule, Global, Module } from '@nestjs/common';
import { LOGGER_MODULE_OPTIONS } from './constants/logger.constants';
import { LoggerOptions } from './interfaces';
import { AppLoggerService } from './services/app.logger.service';
@Global()
@Module({})
export class LoggerModule {
  static forRoot(options?: LoggerOptions): DynamicModule {
    const loggerModuleOptions = {
      provide: LOGGER_MODULE_OPTIONS,
      useValue: options,
    };
    return {
      module: LoggerModule,
      providers: [AppLoggerService, loggerModuleOptions],
      exports: [AppLoggerService],
    };
  }
}
