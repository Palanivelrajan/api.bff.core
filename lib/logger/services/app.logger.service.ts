import { Inject, Injectable, LoggerService, Optional } from '@nestjs/common';
import { join } from 'path';
import { createLogger, format, Logger, transports } from 'winston';
import { LOGGER_MODULE_OPTIONS } from './../constants/logger.constants';
import { LoggerOptions, LogMessage } from './../interfaces';

@Injectable()
export class AppLoggerService implements LoggerService {
  private logger: Logger;
  private IsFileLog = true;
  constructor(
    @Inject(LOGGER_MODULE_OPTIONS)
    private readonly loggerOptions: LoggerOptions,
    @Optional() label?: string,
  ) {
    this.initializeLogger(label);
  }
  initializeLogger(label?: string) {
    if (this.IsFileLog) {
      this.logger = createLogger({
        level: 'info',
        format: format.json(),
        transports: [
          new transports.File({
            dirname: join(this.loggerOptions.appPath, '/log/debug/'),
            filename: 'debug.log',
            level: 'debug',
            maxsize: 100 * 1024,
            maxFiles: 10,
          }),
          new transports.File({
            dirname: join(this.loggerOptions.appPath, '/log/error/'),
            filename: 'error.log',
            level: 'error',
            maxsize: 100 * 1024,
            maxFiles: 10,
          }),
          new transports.File({
            dirname: join(this.loggerOptions.appPath, '/log/info/'),
            filename: 'info.log',
            level: 'info',
            maxsize: 100 * 1024,
            maxFiles: 10,
          }),
          new transports.File({
            dirname: join(this.loggerOptions.appPath, '/log/verbose/'),
            filename: 'verbose.log',
            level: 'verbose',
            maxsize: 100 * 1024,
            maxFiles: 10,
          }),
        ],
      });
    } else {
      this.logger = createLogger({
        level: 'info',
        format: format.json(),
        transports: [
          // new DebugTransport({ level: 'info' }),
          // new ClientApiMetricsTransport({ level: 'info' }),
        ],
      });
    }
  }
  error(message: LogMessage, trace: string) {
    this.logger.log('error', 'error', message);
  }

  warn(message: LogMessage) {
    this.logger.log('warn', 'warn : ', message);
  }

  log(message: LogMessage) {
    this.logger.log('info', 'info :', message);
  }

  verbose(message: LogMessage) {
    this.logger.log('verbose', 'verbose :', message);
  }

  debug(message: LogMessage) {
    this.logger.log('debug', 'debug :', message);
  }
  getlogger(): number {
    return this.logger.transports.length;
  }
}
