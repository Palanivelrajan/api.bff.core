import Transport = require('winston-transport');
export class DebugTransport extends Transport {
  constructor(opts: any) {
    super(opts);
  }
  log(info, callback) {
    setImmediate(() => {
      this.emit('logged', info);
    });
    callback();
  }
}
