import { ReflectMetadata, Global } from '@nestjs/common';

export const RequestTimeout = (...args: string[]) => ReflectMetadata('request-timeout', args);
