
import { AllExceptionsFilter } from './all-exceptions.filter';
import { AppLoggerService } from './../../logger/services/app.logger.service';
import { TestingModule, Test } from '@nestjs/testing';

const mockLogger = { error: jest.fn() }

const mockException = {
    name: 'unknown',
    message: 'This message details'
};

const mockContext: any = {
    switchToHttp: () => ({
        getRequest: () => ({
            url: "mock-url"
        }),
        getResponse: () => {
            const response = {
                code: 0,
                status: code => {
                    response.code = code;
                    return response;
                },
                send: data => {
                    return data;
                }
            };
            return response;
        }
    })
};

describe('AllExceptionFilter', () => {
    let filter: AllExceptionsFilter;
    beforeEach(() => {
        filter = new AllExceptionsFilter(mockLogger as any);
    });
    it('should catch and log the error', () => {
        filter.catch(mockException, mockContext);
        expect(mockLogger.error).toBeCalled();
    });
});