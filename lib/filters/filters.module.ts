import { DynamicModule, Global, Module } from '@nestjs/common';
import { LoggerModule } from '../logger';
import { AllExceptionsFilter } from './all-exceptions/all-exceptions.filter';
import { BadRequestExceptionFilter } from './bad-request-exception/badrequest-exception.filter';
import { FILTER_MODULE_OPTIONS } from './constants/filter.constants';
import { HttpExceptionFilter } from './http-exception/http-exception.filter';
import { FilterOptions } from './interfaces/filter-options.interface';
import { RequestTimeoutExceptionFilter } from './request-timeout-exception/request-timeout-exception.filter';

@Global()
@Module({})
export class FiltersModule {
  static forRoot(options?: FilterOptions): DynamicModule {
    const filterModuleOptions = {
      provide: FILTER_MODULE_OPTIONS,
      useValue: options,
    };
    return {
      module: FiltersModule,
      imports: [LoggerModule],
      providers: [
        AllExceptionsFilter,
        BadRequestExceptionFilter,
        HttpExceptionFilter,
        RequestTimeoutExceptionFilter,
        filterModuleOptions,
      ],
    };
  }
}
