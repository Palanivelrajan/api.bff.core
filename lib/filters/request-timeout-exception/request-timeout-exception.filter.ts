import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpStatus,
  RequestTimeoutException,
} from '@nestjs/common';
import { AppLoggerService } from '../../logger';
@Catch(RequestTimeoutException)
export class RequestTimeoutExceptionFilter implements ExceptionFilter {
  constructor(private logger: AppLoggerService) {}
  catch(exception: RequestTimeoutException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();

    const status =
      exception instanceof RequestTimeoutException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;

    const message = {
      Title: exception.message.error,
      Type: 'Exception - RequestTimeoutException',
      Detail: exception.message,
      Status: '',
    };

    this.logger.error(message, '');

    response.code(status).send({
      statusCode: status,
      timestamp: new Date().toISOString(),
    });
  }
}
