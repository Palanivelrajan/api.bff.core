import { RequestTimeoutExceptionFilter } from './request-timeout-exception.filter';
import { AppLoggerService } from './../../logger/services/app.logger.service';
import { of } from 'rxjs';

const mockLogger = { error: jest.fn() }

const mockException: any = {
  name: 'RequestTimeoutExceptionFilter',
  message: 'This is message details',
  getResponse: jest.fn().mockReturnValue(of('getResponse')),
  getStatus: function () { jest.fn().mockReturnValue(of(404)) }
};

const mockContext: any = {
  switchToHttp: () => ({
    getRequest: () => ({
      url: "mock-url"
    }),
    getResponse: () => {
      const response = {
        code: code => {
          response.code = code;
          return response;
        },
        send: data => {
          return data;
        }
      };
      return response;
    }
  })
};

describe('RequestTimeoutExceptionFilter', () => {
  let filter: RequestTimeoutExceptionFilter;

  beforeEach(() => {
    filter = new RequestTimeoutExceptionFilter(mockLogger as any);
  });

  it('should catch and log the error', () => {
    filter.catch(mockException, mockContext);
    expect(mockLogger.error).toBeCalled();
  });

});