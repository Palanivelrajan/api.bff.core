import { TransformInterceptor } from './transform.interceptor';
import { AppLoggerService } from './../../logger/services/app.logger.service';
import { TestingModule, Test } from '@nestjs/testing';

describe('TransformInterceptor', () => {
    let transformInterceptor: TransformInterceptor;
    let appLoggerService: AppLoggerService;
    beforeEach(async () => {
        const loggerOptions = { appPath: process.cwd() }
        const module: TestingModule = await Test.createTestingModule({
            providers: [TransformInterceptor,
                {
                    provide: AppLoggerService,
                    useValue: new AppLoggerService(loggerOptions, 'Healthcheck'),
                },
            ],
        }).compile();
        transformInterceptor = module.get<TransformInterceptor>(TransformInterceptor);
    });

    it('should be defined', () => {
        expect(transformInterceptor).toBeDefined();
    });
});