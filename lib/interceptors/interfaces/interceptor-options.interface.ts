export interface InterceptorOptions {
  requestTimeout: number;
}
