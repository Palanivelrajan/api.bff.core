import { DynamicModule, Global, Module } from '@nestjs/common';
import { LoggerModule } from '../logger/logger.module';
import { INTERCEPTOR_MODULE_OPTIONS } from './constants/interceptor.constants';
import { InterceptorOptions } from './interfaces/interceptor-options.interface';
import { LoggingInterceptor } from './logger/logging.interceptor';
import { TimeoutInterceptor } from './timeout/timeout.interceptor';
import { TransformInterceptor } from './transform/transform.interceptor';

@Global()
@Module({})
export class InterceptorsModule {
  static forRoot(interceptorOptions?: InterceptorOptions): DynamicModule {
    const interceptorModuleOptions = {
      provide: INTERCEPTOR_MODULE_OPTIONS,
      useValue: interceptorOptions,
    };
    return {
      module: InterceptorsModule,
      imports: [LoggerModule],
      providers: [
        TransformInterceptor,
        LoggingInterceptor,
        TimeoutInterceptor,
        interceptorModuleOptions,
      ],
    };
  }
}
