import { TimeoutInterceptor } from './timeout.interceptor';
import { AppLoggerService } from './../../logger/services/app.logger.service';
import { TestingModule, Test } from '@nestjs/testing';
import { INTERCEPTOR_MODULE_OPTIONS } from '../constants/interceptor.constants';

describe('TimeoutInterceptor', () => {
    let timeoutInterceptor: TimeoutInterceptor;
    let appLoggerService: AppLoggerService;
    const interceptorOptions =  { requestTimeout: 5000 }
    beforeEach(async () => {
        const loggerOptions = { appPath: process.cwd() }
        const module: TestingModule = await Test.createTestingModule({
            providers: [TimeoutInterceptor,
                {
                    provide: AppLoggerService,
                    useValue: new AppLoggerService(loggerOptions, 'Healthcheck'),
                },
                {
                    provide: INTERCEPTOR_MODULE_OPTIONS,
                    useValue: interceptorOptions ,
                },
            ],
        }).compile();
        timeoutInterceptor = module.get<TimeoutInterceptor>(TimeoutInterceptor);
    });

    it('should be defined', () => {
        expect(timeoutInterceptor).toBeDefined();
    });
});