import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
  Inject,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { timeout } from 'rxjs/operators';
import { InterceptorOptions } from '../interfaces/interceptor-options.interface';
import { INTERCEPTOR_MODULE_OPTIONS } from '../constants/interceptor.constants';

@Injectable()
export class TimeoutInterceptor implements NestInterceptor {
  constructor(
    @Inject(INTERCEPTOR_MODULE_OPTIONS)
    private readonly interceptorOptions: InterceptorOptions,
  ) { }
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(timeout(this.interceptorOptions.requestTimeout));
  }
}
