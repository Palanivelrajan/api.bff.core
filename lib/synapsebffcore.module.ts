import { DynamicModule, Global, Module } from '@nestjs/common';
import { FiltersModule } from './filters/filters.module';
import { InterceptorsModule } from './interceptors/interceptors.module';
import { LoggerModule } from './logger';
import { SynapseBFFCoreOptions } from './synapsebffcore-options.interface';
import { DomainApiModule } from './domain-api/';
@Global()
@Module({})
export class SynapseBffCoreModule {
  static forRoot(options?: SynapseBFFCoreOptions): DynamicModule {
    return {
      module: SynapseBffCoreModule,
      imports: [
        LoggerModule.forRoot(options.loggerOptions),
        InterceptorsModule.forRoot(options.interceptorOptions),
        FiltersModule.forRoot(options.filterOptions),
        DomainApiModule.forRoot(options.domainAPIOption),
      ],
      exports: [LoggerModule, InterceptorsModule, FiltersModule, DomainApiModule],
    };
  }
}
