import { FilterOptions } from './filters';
import { InterceptorOptions } from './interceptors';
import { LoggerOptions } from './logger';
import { DomainAPIOptions } from './domain-api';
export interface SynapseBFFCoreOptions {
  filterOptions: FilterOptions;
  interceptorOptions: InterceptorOptions;
  loggerOptions: LoggerOptions;
  domainAPIOption: DomainAPIOptions;

}
